#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE 50
#define NUM_ARRAYS 100

/**@brief Print the grid of 100 50 digit array
 *
 *@param int *pintGrid: Array of numbers to be printed
 *
 *@ret Retruns 0 on success
 */
int printGrid(int *pintGrid)
{
	
	int intY, intX;

	for(intY=0; intY < NUM_ARRAYS; intY++){
		for(intX=0; intX < ARRAY_SIZE; intX++){
			printf("%d,", pintGrid[intY * ARRAY_SIZE + intX]);
		}
		printf("\n");
	}

	return 0;
}

/**@brief Read in a file of 100 50 digit integers
 *
 *Reads in a file that holds one digit integers in a grid
 *
 *@param int *pintGrid: Array to be filled with numbers from a file
 *
 *@ret Returns 0 on success, -1 of failure
 */
int readGrid(int *pintGrid)
{
	FILE* pfIn;
	int intI = 0;
	char chrTemp;
	
	/* open file */
	pfIn = fopen("grid.txt", "r");
	if(!pfIn){
		fprintf(stderr, "Could not open file: grid.txt\n");
		return -1;
	}

	/* read numbers in skipping newlines */
	while((chrTemp = fgetc(pfIn)) != EOF){
		if(chrTemp != '\n'){
			pintGrid[intI] = chrTemp - '0';
			intI++;
		}
	}

	/* close the file */
	if(fclose(pfIn)){ 
		fprintf(stderr, "Error closing file: grid.txt\n");
		return -1;
	}

	return 0;
}

/**@breif Add a fifty digit number to a sum
 *
 *@param int *pintNum: Number to add to the sum
 *@param int *pintSum: Sum to add the number to
 */
void addFiftyDigitNum(int *pintNum, int *pintSum)
{
	int intNumCount = 49;
	int intSumCount = 99;
	int intTemp = 0;

	/* add each digit */
	for(intNumCount = 49;intNumCount>=0;intNumCount--){
		intSumCount = intNumCount + 50;
		intTemp = pintNum[intNumCount] + pintSum[intSumCount];

		pintSum[intSumCount] = intTemp % 10;
		intTemp /=10;

		/* continue to add when carrying the number */
		while(intTemp != 0){
			intSumCount--;
			intTemp += pintSum[intSumCount];
			pintSum[intSumCount] = intTemp % 10;
			intTemp /=10;
		}
	}

	/* print the result */
	for(intSumCount = 0; intSumCount < 100; intSumCount++){
		printf("%d", pintSum[intSumCount]);
	}
	printf("\n");

}

int main(int argc, char* argv[])
{

	int pintGrid[ARRAY_SIZE * NUM_ARRAYS];
	int pintSum[100];
	int intI;
	int *pintNum;
	
	readGrid(pintGrid);
	/*printGrid(pintGrid);*/

	/* initialize the sum grid */
	for(intI=0;intI<100;intI++){
		pintSum[intI] = 0;
	}

	/* add each 50 digit number to sum */
	for(intI = 0; intI < 5000; intI+=50){
		pintNum = pintGrid + intI ;
		addFiftyDigitNum(pintNum, pintSum);
	}
	
	return 0;
}
